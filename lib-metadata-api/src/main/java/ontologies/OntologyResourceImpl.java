/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package ontologies;


/**
 * This implements interface for ontology source associated with a property of section.
 */
public class OntologyResourceImpl implements OntologySource {

    private final String sourceId;
    private final String resource;

    public OntologyResourceImpl(String resource, String sourceId){
        this.resource = resource;
        this.sourceId = sourceId;
    }

    @Override
    public String getSourceId() {
        return sourceId;
    }

    @Override
    public String getResource() {
        return resource;
    }

}
