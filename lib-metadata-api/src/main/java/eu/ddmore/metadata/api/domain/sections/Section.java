/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.api.domain.sections;

/**
 * This is interface for sections in pharmml metadata.
 */
public interface Section {

    /**
     * Gets section number of the section.
     * 
     * @return section number
     */
    String getSectionNumber();

    /**
     * Gets section label associated with the section.
     * 
     * @return section label
     */
    String getSectionLabel();

    /**
     * Gets tooltip associated with the section.
     * 
     * @return tootip
     */
    String getToolTip();

    /**
     * Checks if section is enabled based on fillability enabled option of the section. 
     * 
     * @return Boolean result
     */
    Boolean isEnabled();

    /**
     * Checks if free text is allowed for properties of this section.
     * 
     * @return Boolean result
     */
    Boolean isFreeTextAllowed();

    /**
     * Gets section position which is order of the section at among other sections at their level.
     * For e.g. if Section 5 has 4 subsections as 5.1, 5.2, 5.3, 5.4 then their order should be '1','2','3','4'.
     * 
     * @return section order
     */
    Integer getSectionOrder();

    /**
     * Flag to indicate whether its leaf generic section with properties and concept associated.
     * @return
     */
    boolean isSectionWithProperties();
}
