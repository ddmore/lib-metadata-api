/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.api.domain.properties;

import java.util.List;

import ontologies.OntologySource;

import eu.ddmore.metadata.api.domain.Id;
import eu.ddmore.metadata.api.domain.enums.PropertyRange;
import eu.ddmore.metadata.api.domain.enums.ValueSetType;
import eu.ddmore.metadata.api.domain.values.Value;

/**
 * This class stores property information associated with a section.
 */
public class CompositeProperty implements Property {

    private boolean isRequired;
    private boolean isEnabled;
    //from the information we have at this moment default flag value is true
    private boolean isPharmml=true;
    private ValueSetType valueSetType;
    private Id propertyId;
    private Id conceptId;
    private List<OntologySource> ontologySource;
    private List<Value> values;
    private PropertyRange range;
    private String freeText = "";

    public Boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean isRequired) {
        this.isRequired = isRequired;
    }

    public Boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public Id getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Id propertyId) {
        this.propertyId = propertyId;
    }

    public ValueSetType getValueSetType() {
        return valueSetType;
    }

    public void setValueSetType(ValueSetType valueSetType) {
        this.valueSetType = valueSetType;
    }

    public PropertyRange getRange() {
        return range;
    }

    public void setRange(PropertyRange range) {
        this.range = range;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    @Override
    public Id getConceptId() {
        return conceptId;
    }

    public void setConceptId(Id concept) {
        this.conceptId = concept;
    }

    public List<OntologySource> getOntologySource() {
        return ontologySource;
    }

    public void setOntologySource(List<OntologySource> ontologySource) {
        this.ontologySource = ontologySource;
    }

    @Override
    public Boolean isPharmml() {
        return isPharmml;
    }

    public void setPharmml(boolean isPharmml) {
        this.isPharmml = isPharmml;
    }

    @Override
    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }
}
