/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.api.domain.values;

import org.apache.commons.lang.StringUtils;

import eu.ddmore.metadata.api.domain.Id;

/**
 * This class stores the lib-metadata value details.
 * 
 */
public class ValueInfo {

    private Id valueId;
    private String valueRank;
    Boolean valueTree = false;

    public ValueInfo(){
    }

    public ValueInfo(Id valueId){
        setValueId(valueId);
    }

    public ValueInfo(Id valueId, String valueRank){
        setValueId(valueId);
        if(StringUtils.isNotEmpty(valueRank)){
            setValueRank(valueRank);
            setValueTree(true);
        }
    }

    public Id getValueId() {
        return valueId;
    }

    public void setValueId(Id valueId) {
        this.valueId = valueId;
    }

    public String getValueRank() {
        return valueRank;
    }

    public void setValueRank(String valueRank) {
        this.valueRank = valueRank;
    }

    public Boolean isValueTree() {
        return valueTree;
    }

    public void setValueTree(Boolean valueTree) {
        this.valueTree = valueTree;
    }

}
