/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.api.domain.sections;

import java.util.ArrayList;
import java.util.List;

import eu.ddmore.metadata.api.domain.properties.Property;

/**
 * Composite section stores list of generic sections associated to a section.
 */
public class CompositeSection implements Section {

    private final String sectionNumber;
    private final String sectionLabel;
    private String toolTip;
    private Integer sectionOrder;
    private Boolean isEnabled = false;
    private Boolean isFreeTextAllowed = false;
    private final List<Section> sections = new ArrayList<>();

    public CompositeSection(String sectionNumber, String sectionLabel){
        this.sectionNumber = sectionNumber;
        this.sectionLabel = sectionLabel;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void addSection(Section section){
        sections.add(section);
    }

    public void addPropertyToSection(GenericSection section, Property property){
        section.addProperty(property);
    }

    public String getSectionNumber() {
        return sectionNumber;
    }

    public String getSectionLabel() {
        return sectionLabel;
    }

    @Override
    public String getToolTip() {
        return toolTip;
    }

    public void setToolTip(String toolTip) {
        this.toolTip = toolTip;
    }

    @Override
    public boolean isSectionWithProperties() {
        return false;
    }

    @Override
    public Integer getSectionOrder() {
        return sectionOrder;
    }

    @Override
    public Boolean isFreeTextAllowed() {
        return isFreeTextAllowed;
    }

    public void setIsFreeTextAllowed(Boolean isFreeTextAllowed) {
        this.isFreeTextAllowed = isFreeTextAllowed;
    }

    public void setSectionOrder(Integer sectionOrder) {
        this.sectionOrder = sectionOrder;
    }

    @Override
    public Boolean isEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
}