/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.api;

import java.util.List;

import ontologies.OntologySource;

import eu.ddmore.metadata.api.domain.Id;
import eu.ddmore.metadata.api.domain.properties.Property;
import eu.ddmore.metadata.api.domain.sections.Section;
import eu.ddmore.metadata.api.domain.values.Value;

/**
 * This is interface for metadata information service
 */
public interface MetadataInformationService {

    /**
     * Gets metadata information in form of list of sections for the section with partial information provided.
     *  
     * @param section
     * @return list of generic section
     */
    List<Section> getMetadataInformationFor(Section section);

    /**
     * Gets list of sections associated with the concept id provided.
     * 
     * @param conceptId
     * @return list of sections
     */
    List<Section> findSectionsForConcept(Id conceptId);

    /**
     * Gets the properties for a section provided as query.
     * 
     * @param section
     * @return list of properties
     */
    List<Property> findPropertiesForSection(Section section);

    /**
     * Gets values associated with the property.
     * <p>For List values : list of values
     * <p>For Tree values : values in hierarchical tree structure
     * <p>for Text values : no values at the moment (empty list).
     * @param property
     * @return list of values
     */
    List<Value> findValuesForProperty(Property property);

    /**
     * Gets ontology sources associated with the property. 
     * @param property
     * @return list of ontology resources
     */
    List<OntologySource> findOntologyResourcesForProperty(Property property);

    /**
     * Find root sections for a concept specified with all the subsections associated.
     * @param conceptId
     * @return list of sections
     */
    List<Section> findPopulatedRootSectionsForConcept(Id conceptId);

    /**
     * Gets all the root sections with all the subsections and other details associated.
     * @return list of sections
     */
    List<Section> getAllPopulatedRootSections();

    /**
     * Find associated sub sections for section specified. 
     * @param section
     * @return List of sections
     */
    List<Section> findSubSectionsForSection(Section section);

    /**
     * Find associated sub sections for section specified. 
     * @param section
     * @return List of sections
     */
    List<Section> getAllRootSections();

}
