/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.api.domain.properties;

import eu.ddmore.metadata.api.domain.Id;
import eu.ddmore.metadata.api.domain.enums.PropertyRange;
import eu.ddmore.metadata.api.domain.enums.ValueSetType;

/**
 * This provides access to property information associated with a section.
 */
public interface Property {

    /**
     * Checks if property is required based on fillability mode.
     * 
     * @return Boolean result
     */
    Boolean isRequired();

    /**
     * Checks if property is enabled based on fillability enabled option of the property. 
     * 
     * @return Boolean result
     */
    Boolean isEnabled();

    /**
     * Returns URI/Label information of property.
     * 
     * @return Id property id
     */
    Id getPropertyId();

    /**
     * Gets value set type corresponding to a property.
     * 
     * @return ValueSetType indicating value type
     */
    ValueSetType getValueSetType();

    /**
     * Returns property range based on fallibility choice mentioned .
     * 
     * @return PropertyRange result
     */
    PropertyRange getRange();

    /**
     * Gets concept id of the property.
     * 
     * @return concept id
     */
    Id getConceptId();

    /**
     * Checks if property is pharmml property based on information available for the property.
     * 
     * @return boolean result
     */
    Boolean isPharmml();

    /**
     * Gets free text associated with the property if section has allowed free text for its properties.
     * This can be empty as its optional value.
     * 
     * @return freetext string if available
     */
    String getFreeText();
}
