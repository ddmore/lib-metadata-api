/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.api.domain.values;

import eu.ddmore.metadata.api.domain.Id;

public class SimpleValue implements Value {

    ValueInfo valueInfo;

    public SimpleValue(Id valueId){
        valueInfo = new ValueInfo(valueId);
    }

    public SimpleValue(String label, String uri) {
        valueInfo = new ValueInfo(new Id(label, uri));
    }

    public Id getValueId(){
        return valueInfo.getValueId();
    }

    @Override
    public Boolean isValueTree() {
        return valueInfo.isValueTree();
    }

    @Override
    public String getValueRank() {
        return valueInfo.getValueRank();
    }

}