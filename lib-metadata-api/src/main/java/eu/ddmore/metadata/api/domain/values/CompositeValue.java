/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.api.domain.values;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import eu.ddmore.metadata.api.domain.Id;

/**
 * This class represents composite value (might be called as subterms) which contains value or list of values.
 */
public class CompositeValue implements Value {

    ValueInfo valueInfo;

    private Map<String, Value> values = new TreeMap<String, Value>();

    /**
     * This constructor is used as base list of subterms (composite values) in case of "tree" value set type.
     */
    public CompositeValue(){
    }

    public CompositeValue(Id valueId, String valueRank) {
        valueInfo = new ValueInfo(valueId, valueRank);
    }

    public CompositeValue(String label, String uri, String valueRank) {
        Id valueId = new Id(label, uri);
        valueInfo = new ValueInfo(valueId, valueRank);
    }

    public void addValue(Value value){
        values.put(value.getValueRank(), value);
    }

    public Value getValueWithRank(String valueRank){
        return values.get(valueRank);
    }

    public Id getValueId() {
        return valueInfo.getValueId();
    }

    @Override
    public Boolean isValueTree() {
        return valueInfo.isValueTree();
    }

    public List<Value> getValues() {
        return new ArrayList<Value>(values.values());
    }

    public void setValues(Map<String, Value> values) {
        this.values = values;
    }

    @Override
    public String getValueRank() {
        return valueInfo.getValueRank();
    }

    public void setValueRank(String valueRank){
        valueInfo.setValueRank(valueRank);
    }
}
