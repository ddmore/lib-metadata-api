/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.impl;

import java.util.ArrayList;
import java.util.List;

import ontologies.OntologySource;

import eu.ddmore.metadata.api.MetadataInformationService;
import eu.ddmore.metadata.api.domain.Id;
import eu.ddmore.metadata.api.domain.enums.ValueSetType;
import eu.ddmore.metadata.api.domain.properties.CompositeProperty;
import eu.ddmore.metadata.api.domain.properties.Property;
import eu.ddmore.metadata.api.domain.sections.CompositeSection;
import eu.ddmore.metadata.api.domain.sections.GenericSection;
import eu.ddmore.metadata.api.domain.sections.Section;
import eu.ddmore.metadata.api.domain.values.Value;
import eu.ddmore.metadata.sparql.SparqlQueryExecutor;

/**
 * This class implements metadata information service for the pharmml metadata.
 * Currently it queries upon open-physiology server by default.
 */
public class MetadataInformationServiceImpl implements MetadataInformationService {

    private SparqlQueryExecutor executor;

    public MetadataInformationServiceImpl(SparqlQueryExecutor executor){
        this.executor = executor;
    }

    @Override
    public List<Section> getMetadataInformationFor(Section section){
        Id conceptId = executor.getConceptForSection(section);
        CompositeProperty property = new CompositeProperty(); 
        property.setConceptId(conceptId);

        GenericSection genSection = new GenericSection(section.getSectionNumber(), section.getSectionLabel());
        genSection.addProperty(property);

        List<Section> resultingSections = new ArrayList<>();
        resultingSections.add(genSection);
        return resultingSections;
    }

    @Override
    public List<Section> findSectionsForConcept(Id conceptId) {
        List<Section> resultingCaterogies = new ArrayList<>();

        resultingCaterogies = executor.getSectionsForConcept(conceptId);
        return resultingCaterogies;
    }

    @Override
    public List<Section> findSubSectionsForSection(Section section) {
        List<Section> subSections = new ArrayList<Section>();

        subSections = executor.getSubSectionsForSection(section);
        return subSections;
    }

    @Override
    public List<Property> findPropertiesForSection(Section section) {
        List<Property> properties = new ArrayList<>();

        properties = executor.getPropertiesForSection(section);
        return properties;
    }

    @Override
    public List<Value> findValuesForProperty(Property property) {
        List<Value> values = new ArrayList<>();

        values = executor.getValuesForProperty(property);
        return values;
    }

    @Override
    public List<OntologySource> findOntologyResourcesForProperty(Property property) {
        List<OntologySource> ontologySources = new ArrayList<>();

        ontologySources = executor.getOntologyForProperty(property);
        return ontologySources;
    }

    @Override
    public List<Section> findPopulatedRootSectionsForConcept(Id conceptId) {
        List<Section> resultingSections = new ArrayList<>();

        resultingSections = executor.getRootSectionsWithConcept(conceptId);
        for(Section section : resultingSections){
            populateSectionWithSubSections(section);
        }
        return resultingSections;
    }

    @Override
    public List<Section> getAllRootSections(){
        List<Section> resultingSections = new ArrayList<>();

        resultingSections = executor.getAllRootSections();
        return resultingSections;
    }

    @Override
    public List<Section> getAllPopulatedRootSections() {
        List<Section> resultingSections = new ArrayList<>();

        resultingSections = executor.getAllRootSections();
        for(Section section : resultingSections){
            populateSectionWithSubSections(section);
        }
        return resultingSections;
    }

    private void populateSectionWithSubSections(Section section){
        if(!section.isSectionWithProperties()){
            List<Section> resultingSections = findSubSectionsForSection(section);

            for(Section subSection : resultingSections){

                if(!subSection.isSectionWithProperties()){
                    populateSectionWithSubSections(subSection);
                    ((CompositeSection) section).addSection(subSection);
                }else {
                    GenericSection genericSection = (GenericSection) subSection;
                    List<Property> properties = findPropertiesForSection(genericSection);
                    if(!properties.isEmpty()){
                        for(Property property: properties){
                            CompositeProperty prop = (CompositeProperty) property;
                            if(prop.getValueSetType().equals(ValueSetType.ONTOLOGY)){
                                prop.setOntologySource(findOntologyResourcesForProperty(prop));
                            }else{
                                prop.setValues(findValuesForProperty(prop));
                            }
                            genericSection.addProperty(prop);
                        }
                    }
                    ((CompositeSection) section).addSection(genericSection);
                }
            }

        }
    }
}
