/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.sparql;

import eu.ddmore.metadata.api.domain.Id;
import eu.ddmore.metadata.api.domain.properties.Property;
import eu.ddmore.metadata.api.domain.sections.Section;

/**
 * Spaql Queries used by libmetadata
 */
public class Queries {

    private static final String ANNOTATABLE_WITH_NON_PHARMML_PREDICATE = " wat:annotatablewithNonPharmMLSubstitution ";
    private static final String ANNOTATABLE_WITH_PREDICATE = " wat:annotatablewith ";
    private static final String EUR_DOC_LABEL = "EUR document";
    public static final String _Q = "?";

    private static final String PREFIXES = "PREFIX pmlo: <http://www.pharmml.org/ontology/> " +
            "PREFIX pml: <http://www.pharmml.org/2013/10/PharmMLMetadata#> " +
            "PREFIX wat: <http://www.ddmore.org/ontologies/webannotationtool#> "+
            "PREFIX rdfns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
            "PREFIX eur: <http://www.ddmore.org/ontologies/metadataeur#> "+
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
            "PREFIX ps: <http://www.pharmml.org/2013/10/PharmMLMetadata#>";

    public static String appendPrefix(String variable){
        if(!variable.startsWith(_Q)){
            variable = _Q+variable;
        }
        return variable;
    }

    /**
     *  Creates query to get concept for section with section number.
     *
        select ?varConceptLabel ?varConceptUri where {
             ?x eur:section-number "<sectionNum>" .
             ?x wat:hasMetadataSlot ?y .
             ?varConceptUri wat:annotatablewith ?y .
             ?varConceptUri rdfs:label ?varConceptLabel }
     *  
     * @param varConceptLabel
     * @param varConceptUri
     * @param sectionNum
     * @return query string
     */
    public static String queryToGetConceptForSection(String varConceptLabel, String varConceptUri, final String sectionNum) {
        varConceptLabel = appendPrefix(varConceptLabel);
        varConceptUri = appendPrefix(varConceptUri);

        String query = PREFIXES+ " select "+varConceptLabel + " "+ varConceptUri+
                " where { " +
                "?x eur:section-number \""+sectionNum+"\" . " +
                "?x wat:hasMetadataSlot ?y . "+
                varConceptUri+" wat:annotatablewith ?y . " +
                varConceptUri+" rdfs:label "+ varConceptLabel +" }";
        return query;
    }

    /**
     * Creates query to get sections for concept id.
     * 
         select ?varSectionNumber ?varSectionLabel ?varSectionEnabled ?varSectionToolTip ?varPositionOrder where {
             ?x eur:section-number ?varSectionNumber .
             ?x rdfs:label ?varSectionLabel .
             ?x wat:tooltip ?varSectionToolTip .
             ?x wat:fillability-enabled ?varSectionEnabled .
             OPTIONAL {?x eur:section-position ?varPositionOrder . }
             ?x wat:hasMetadataSlot ?y .
             ?z wat:annotatablewith ?y .
             ?z rdfs:label "<concept>" }
     * 
     * @param concept
     * @param varSectionNumber
     * @param varSectionLabel
     * @param varPositionOrder
     * @param varSectionToolTip
     * @return
     */
    public static String queryToGetSectionForConcept(Id concept, String varSectionNumber, String varSectionLabel, 
            String varSectionEnabled, String varPositionOrder, String varSectionToolTip) {
        varSectionNumber= appendPrefix(varSectionNumber);
        varSectionEnabled = appendPrefix(varSectionEnabled);
        varSectionLabel = appendPrefix(varSectionLabel);
        varSectionToolTip = appendPrefix(varSectionToolTip);
        varPositionOrder = appendPrefix(varPositionOrder);

        String query = PREFIXES+ " select "+varSectionNumber+ " "+
                varSectionLabel+ " " + varSectionEnabled +" " + varSectionToolTip +" "+varPositionOrder+
                " where { " +
                "?x eur:section-number "+varSectionNumber+" . " +
                "?x rdfs:label "+varSectionLabel+" . " +
                "?x wat:tooltip "+varSectionToolTip +" . " +
                "?x wat:fillability-enabled" +varSectionEnabled +" . " +
                "OPTIONAL {?x eur:section-position "+varPositionOrder+" . } "+
                "?x wat:hasMetadataSlot ?y . " +
                "?z wat:annotatablewith ?y . " +
                "?z rdfs:label \""+ concept.getLabel() + "\" }";
        return query;
    }

    /**
     * Creates query to get all root sections.
     * 
        select ?varSectionNumber ?varSectionLabel ?varSectionEnabled ?varSectionToolTip where {
            ?doc rdfs:label "EUR document" . 
            ?x eur:section-of ?doc .
            ?x eur:section-number ?varSectionNumber .
            ?x rdfs:label ?varSectionLabel .
            ?x wat:fillability-enabled ?varSectionEnabled .
            OPTIONAL {?x eur:section-position  ?varPositionOrder .}
            ?x wat:tooltip ?varSectionToolTip . }
     * 
     * @param varSectionNumber
     * @param varSectionLabel
     * @param varPositionOrder
     * @param varSectionToolTip
     * @return query
     */
    public static String queryToGetAllRootSections(String varSectionNumber, String varSectionLabel, 
            String varSectionEnabled, String varPositionOrder, String varSectionToolTip) {
        varSectionNumber= appendPrefix(varSectionNumber);
        varSectionLabel = appendPrefix(varSectionLabel);
        varSectionEnabled = appendPrefix(varSectionEnabled);
        varSectionToolTip = appendPrefix(varSectionToolTip);
        varPositionOrder = appendPrefix(varPositionOrder);

        String query = PREFIXES+ " select "+varSectionNumber+ " "+
                varSectionLabel+ " " + varSectionEnabled + " " + varSectionToolTip +
                " where { " +
                "?doc rdfs:label \""+EUR_DOC_LABEL+"\" . " + 
                "?x eur:section-of ?doc . " +
                "?x eur:section-number "+varSectionNumber+" . " +
                "?x rdfs:label "+varSectionLabel+" . " +
                "?x wat:fillability-enabled" +varSectionEnabled +" . " +
                "OPTIONAL {?x eur:section-position  "+varPositionOrder+" .} "+
                "?x wat:tooltip "+varSectionToolTip +" . }";
        return query;
    }

    /**
     * Creates query to get root sections associated with concept provided.
     * 
        select distinct ?varSectionNumber ?varSectionEnabled ?varSectionLabel ?varSectionToolTip where {
            ?doc rdfs:label "EUR document" . 
            ?a eur:section-of ?doc .
            ?a eur:section-number ?varSectionNumber .
            ?a rdfs:label ?varSectionLabel .
            ?a wat:tooltip ?varSectionToolTip .
            ?a wat:fillability-enabled ?varSectionEnabled .
            OPTIONAL { ?x eur:section-position ?varPositionOrder . }
            ?x eur:section-of+ ?a .
            ?x wat:hasMetadataSlot ?y .
            ?z wat:annotatablewith ?y .
            ?z rdfs:label "<concept>" . }
     * 
     * @param concept
     * @param varSectionNumber
     * @param varSectionLabel
     * @param varSectionToolTip
     * @return query
     */
    public static String queryToGetRootSectionsWithConcept(Id concept, String varSectionNumber, 
            String varSectionLabel, String varSectionEnabled, String varPositionOrder, String varSectionToolTip){
        varSectionNumber= appendPrefix(varSectionNumber);
        varSectionLabel = appendPrefix(varSectionLabel);
        varSectionEnabled = appendPrefix(varSectionEnabled);
        varSectionToolTip = appendPrefix(varSectionToolTip);
        varPositionOrder = appendPrefix(varPositionOrder);

        String query = PREFIXES+ " select distinct "+varSectionNumber+ " "+
                varSectionLabel+ " " + varSectionEnabled + " " + varSectionToolTip +
                " where { " +
                "?doc rdfs:label \""+EUR_DOC_LABEL+"\" . " + 
                "?a eur:section-of ?doc . " +
                "?a eur:section-number "+varSectionNumber+" . " +
                "?a rdfs:label "+varSectionLabel+" . " +
                "?a wat:tooltip "+varSectionToolTip +" . " +
                "?a wat:fillability-enabled" +varSectionEnabled +" . " +
                "OPTIONAL {?x eur:section-position  "+varPositionOrder+" . } "+
                "?x eur:section-of+ ?a . " +
                "?x wat:hasMetadataSlot ?y . " +
                "?z wat:annotatablewith ?y . " +
                "?z rdfs:label \""+ concept.getLabel() +"\" . }";
        return query;
    }

    /**
     * Creates query to get sub sections associated with section.
     * 
        select ?varSectionNumber ?varSectionLabel ?varSectionEnabled ?varSectionToolTip ?varIsFreeTextAllowed ?varPositionOrder ?varPropUri where {
            ?a eur:section-number "<SectionNumber>" .
                ?x eur:section-of ?a .
                ?x eur:section-number ?varSectionNumber .
                ?x rdfs:label ?varSectionLabel .
                ?x wat:tooltip ?varSectionToolTip .
                ?x wat:allowsAlternateFreetext ?varIsFreeTextAllowed .
                ?x wat:fillability-enabled ?varSectionEnabled .
                OPTIONAL { ?x eur:section-position  ?varPositionOrder . }
                OPTIONAL { ?x wat:hasMetadataSlot ?varPropUri . } 
        }
     * 
     * @param section
     * @param varSectionNumber
     * @param varSectionLabel
     * @param varSectionToolTip
     * @param varPositionOrder
     * @param varPropUri
     * @return query
     */
    public static String queryToGetSubSectionsOfSection(Section section, String varSectionNumber, 
            String varSectionLabel, String varSectionEnabled, String varSectionToolTip, String varIsFreeTextAllowed, String varPositionOrder, String varPropUri) {
        varSectionNumber= appendPrefix(varSectionNumber);
        varSectionLabel = appendPrefix(varSectionLabel);
        varSectionEnabled = appendPrefix(varSectionEnabled);
        varSectionToolTip = appendPrefix(varSectionToolTip);
        varIsFreeTextAllowed = appendPrefix(varIsFreeTextAllowed);
        varPositionOrder = appendPrefix(varPositionOrder);
        varPropUri= appendPrefix(varPropUri);

        String query = PREFIXES+ " select "+varSectionNumber+ " "+
                varSectionLabel+ " " + varSectionEnabled + " " + varSectionToolTip +" "+ varIsFreeTextAllowed +" "+
                varPositionOrder+" "+varPropUri+
                " where { " +
                "?a eur:section-number \""+section.getSectionNumber()+"\" . " +
                "?x eur:section-of ?a . " +
                "?x eur:section-number "+varSectionNumber+" . " +
                "?x rdfs:label "+varSectionLabel+" . " +
                "?x wat:tooltip "+varSectionToolTip +" . " +
                "?x wat:allowsAlternateFreetext "+varIsFreeTextAllowed +" . " +
                "?x wat:fillability-enabled " +varSectionEnabled +" . " +
                "OPTIONAL { ?x eur:section-position  "+varPositionOrder+" .} "+
                "OPTIONAL { ?x wat:hasMetadataSlot "+varPropUri+". } }";
        return query;
    }

    /**
     * Creates query to get properties associated for section provided.
     * 
        select ?varpropUriForPharmml ?varPropUriForNonPharmml where {
            ?sec_uri wat:allowsAlternateFreetext "false" .
            ?sectionUri eur:section-number "<SectionNumber>" .
            ?sectionUri wat:hasMetadataSlot ?varpropUriForPharmml .
            ?varpropUriForPharmml rdfs:label ?varPropLabelForPharmml .
            ?sectionUri wat:hasMetadataSlotSubstitute ?varPropUriForNonPharmml .
            ?varPropUriForNonPharmml rdfs:label ?varPropLabelForNonPharmml .
        }

     * @param section
     * @param varPropUriForPharmml
     * @param varPropUriForNonPharmml
     * @param varPropLabelForPharmml
     * @param varPropLabelForNonPharmml
     * @return
     */
    public static String queryToGetPropertiesForSection(Section section, 
            String varPropUriForPharmml, String varPropUriForNonPharmml,
            String varPropLabelForPharmml, String varPropLabelForNonPharmml) {

        varPropUriForPharmml = appendPrefix(varPropUriForPharmml);
        varPropLabelForPharmml = appendPrefix(varPropLabelForPharmml);

        varPropUriForNonPharmml = appendPrefix(varPropUriForNonPharmml);
        varPropLabelForNonPharmml = appendPrefix(varPropLabelForNonPharmml);

        String query = PREFIXES+ " select "+varPropUriForPharmml+" "+varPropLabelForPharmml+
                " "+varPropUriForNonPharmml+" "+varPropLabelForNonPharmml+" where { " +
                "?sectionUri eur:section-number \""+section.getSectionNumber()+"\" . "+ 
                "?sectionUri wat:hasMetadataSlot "+ varPropUriForPharmml+ " . "+
                varPropUriForPharmml+" rdfs:label "+varPropLabelForPharmml+" . "+
                "?sectionUri wat:hasMetadataSlotSubstitute "+ varPropUriForNonPharmml+" . "+
                varPropUriForNonPharmml+" rdfs:label "+varPropLabelForNonPharmml+" .}";

        return query;
    }

    /**
     *  
        select ?varIsRequired ?varIsEnabled ?varValSetType ?varPropRange 
                ?varPropUriFreeText ?varConceptLabel ?varConceptUri 
        where {
            <propertyUri> wat:fillability-mode ?varIsRequired .
            <propertyUri> wat:fillability-enabled ?varIsEnabled .
            <propertyUri> wat:fillability-choices ?varPropRange .
            <propertyUri> wat:displayValueType ?varValSetType .
            optional {?varpropUri ps:alternateFreetextProperty ?varPropUriFreeText} .
            ?varConceptUri wat:annotatablewith <propertyUri> .
            ?varConceptUri rdfs:label ?varConceptLabel .
        }
     * 
     * @param property
     * @param varIsRequired
     * @param varIsEnabled
     * @param varValSetType
     * @param varPropRange
     * @param varPropUriFreeText
     * @param varConceptLabel
     * @param varConceptUri
     * @return
     */
    public static String queryTogetPropertyInformation(
            Property property, String varIsRequired, 
            String varIsEnabled, String varValSetType, String varPropRange, String varPropUriFreeText, 
            String varConceptLabel, String varConceptUri){

        String propUri = "<"+property.getPropertyId().getUri()+">";
        varIsRequired = appendPrefix(varIsRequired);
        varIsEnabled = appendPrefix(varIsEnabled);
        varValSetType = appendPrefix(varValSetType);
        varPropRange = appendPrefix(varPropRange);
        varPropUriFreeText = appendPrefix(varPropUriFreeText);
        varConceptLabel = appendPrefix(varConceptLabel);
        varConceptUri = appendPrefix(varConceptUri);

        String conceptPredicate = (property.isPharmml())? ANNOTATABLE_WITH_PREDICATE : ANNOTATABLE_WITH_NON_PHARMML_PREDICATE;

        String query = PREFIXES+ " select "+varIsRequired+" "+varIsEnabled+" "+varValSetType+
                " "+varPropRange+" "+varPropUriFreeText+" "+varConceptLabel+" "+varConceptUri
                +" where { " +
                propUri+" wat:fillability-mode "+varIsRequired +" ."+
                propUri+" wat:fillability-enabled "+varIsEnabled +" ."+
                propUri+" wat:fillability-choices "+varPropRange +" ."+
                propUri+" wat:displayValueType "+varValSetType+" . "+
                "OPTIONAL {"+propUri+ " ps:alternateFreetextProperty "+ varPropUriFreeText+"} ."+
                varConceptUri+conceptPredicate+ propUri +" . "+
                "OPTIONAL {"+varConceptUri+" rdfs:label "+varConceptLabel+".} .}";
        return query;
    }

    /**
     * Creates query to get ontology source associated with property.
     * 
        select distinct ?varOntologyResource ?varOntologyId where { 
            <propUri> wat:slot-has-ontology-source-resource ?varOntologyResource .
            <propUri> wat:slot-has-ontology-source-id ?varOntologyId }
     * 
     * @param propUri
     * @param varOntologyResource
     * @param varOntologyId
     * @return query string
     */
    public static String queryToGetOntologySourceForProperty(final String propUri, String varOntologyResource, String varOntologyId) {

        varOntologyResource= appendPrefix(varOntologyResource);
        varOntologyId = appendPrefix(varOntologyId);

        String query = PREFIXES+ " select distinct "+ varOntologyResource+ " "+varOntologyId 
                +" where { " + 
                propUri +" wat:slot-has-ontology-source-resource "+ varOntologyResource+" . " +
                propUri +" wat:slot-has-ontology-source-id "+ varOntologyId+" }";
        return query;
    }

    /**
     * Creates query to get values of list type as well as text type associated with property.
     * 
        select distinct ?varValueLabel ?varValueUri where {
                <propUri> wat:displayedvalue ?VAL .
                ?VAL rdfns:first ?varValueUri .
                ?VAL rdfns:rest ?r .
                ?r rdfns:first ?varValueLabel . }
     * 
     * @param propUri
     * @param varValueLabel
     * @param varValueUri
     * @return query string
     */
    public static String queryToGetListValuesForProperty(final String propUri, String varValueLabel, String varValueUri) {

        varValueLabel = appendPrefix(varValueLabel);
        varValueUri = appendPrefix(varValueUri);

        String query = PREFIXES+ " select distinct "+varValueLabel+" "+varValueUri
                +" where { " +
                propUri+" wat:displayedvalue ?VAL . "+
                "?VAL rdfns:first "+ varValueUri +" . "+
                "?VAL rdfns:rest ?r . "+
                "?r rdfns:first "+varValueLabel+" . }";
        return query;
    }

    /**
     * Creates query to get values of textlist type as well as text type associated with property.
     * 
        select distinct ?varValueLabel where {
                <propUri> wat:displayedvalueText ?varValueLabel .
                }
     * 
     * @param propUri
     * @param varValueLabel
     * @param varValueUri
     * @return query string
     */
    public static String queryToGetTextListValuesForProperty(final String propUri, String varValueLabel, String varValueUri) {

        varValueLabel = appendPrefix(varValueLabel);
        varValueUri = appendPrefix(varValueUri);

        String query = PREFIXES+ " select distinct "+varValueLabel+" where { " +
                propUri+" wat:displayedvalueText "+ varValueLabel +" .  }";
        return query;
    }

    /**
     * Creates query to get values of tree type associated with property.
     * 
     * select distinct ?varValueLabel ?varValueUri ?varValueRank where {
                <propUri> wat:displayedvalueranked ?VAL .
                ?VAL rdfns:first ?varValueUri .
                ?VAL rdfns:rest ?rest1 .
                ?rest1 rdfns:first ?varValueLabel .
                ?rest1 rdfns:rest ?rest2 .
                ?rest2 rdfns:first ?varValueRank . }
     * 
     * @param propUri
     * @param varValueLabel
     * @param varValueUri
     * @return query string
     */
    public static String queryToGetTreeValuesForProperty(final String propUri, String varValueLabel, String varValueUri, String varValueRank) {

        varValueLabel = appendPrefix(varValueLabel);
        varValueUri = appendPrefix(varValueUri);
        varValueRank = appendPrefix(varValueRank);

        String query = PREFIXES+ " select distinct "+varValueLabel+" "+varValueUri +" "+ varValueRank
                +" where { " +
                propUri+" wat:displayedvalueranked ?VAL . "+
                "?VAL rdfns:first "+ varValueUri +" . "+
                "?VAL rdfns:rest ?rest1 . "+
                "?rest1 rdfns:first "+ varValueLabel+" . " +
                "?rest1 rdfns:rest ?rest2 . " +
                "?rest2 rdfns:first " + varValueRank +" . }";
        return query;
    }
}
