/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.sparql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ontologies.OntologyResourceImpl;
import ontologies.OntologySource;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.RDFNode;

import eu.ddmore.metadata.api.domain.Id;
import eu.ddmore.metadata.api.domain.enums.PropertyRange;
import eu.ddmore.metadata.api.domain.enums.ValueSetType;
import eu.ddmore.metadata.api.domain.properties.CompositeProperty;
import eu.ddmore.metadata.api.domain.properties.Property;
import eu.ddmore.metadata.api.domain.sections.CompositeSection;
import eu.ddmore.metadata.api.domain.sections.GenericSection;
import eu.ddmore.metadata.api.domain.sections.Section;
import eu.ddmore.metadata.api.domain.values.CompositeValue;
import eu.ddmore.metadata.api.domain.values.SimpleValue;
import eu.ddmore.metadata.api.domain.values.Value;

/**
 * Sparql query executor helps with number of functions which 
 * execute sparql query and return the result in form of domain objects.
 */
public class SparqlQueryExecutor {

    private final SparqlQueryService queryService;

    private final String sectionNum = QueryConst.SEC_NUM.toString();
    private final String sectionLabel = QueryConst.LABEL.toString();
    private final String positionOrder = QueryConst.ORDER.toString();
    private final String sectionToolTip = QueryConst.TOOLTIP.toString();
    private final String sectionEnabled = QueryConst.IS_ENABLED.toString();
    private final String varIsFreeTextAllowed = QueryConst.SEC_.toString() + QueryConst.FREETEXT;
    private final String uri = QueryConst.URI.toString();
    private final String label= QueryConst.LABEL.toString();

    public SparqlQueryExecutor(SparqlQueryService queryService){
        this.queryService = queryService;
    }

    /**
     * Gets the concept associated with the section and adds it to the section.
     * 
     * @param section containing querying information
     * @return Id associated concept id
     */
    public Id getConceptForSection(Section section){
        final String sectionNum = section.getSectionNumber();

        String query = Queries.queryToGetConceptForSection(label, uri, sectionNum);

        List<QuerySolution> results = queryService.executeQuery(query);
        QuerySolution soln = results.get(0);
        RDFNode conceptUriNode = soln.get(uri) ;
        RDFNode conceptLabelNode = soln.get(label) ;

        if(conceptUriNode == null || conceptLabelNode == null){
            throw new IllegalArgumentException("No concept is available for the section provided with section : "+section);
        }

        if(results.size()>1) {
            throw new IllegalArgumentException("Given section could not have more than one concept associated");
        }

        String label = conceptLabelNode.asNode().getLiteral().toString();
        String uri = conceptUriNode.asNode().getURI().toString();
        Id conceptId = new Id(label, uri);

        return conceptId;
    }

    /**
     * Get list of sections associated with the concept provided
     * @param concept
     * @return list of sections
     */
    public List<Section> getSectionsForConcept(Id concept){
        List<Section> sections = new ArrayList<>();

        String query = Queries.queryToGetSectionForConcept(concept, sectionNum, sectionLabel, sectionEnabled, positionOrder, sectionToolTip); 
        List<QuerySolution> results = queryService.executeQuery(query);
        for (QuerySolution soln : results) {

            String sectionNumber = soln.get(sectionNum).toString();
            String secLabel = soln.get(sectionLabel).toString();
            String toolTip = soln.get(sectionToolTip).toString();
            String isEnabled = soln.get(sectionEnabled).toString();
            String secOrder = soln.get(positionOrder).toString();

            GenericSection section = new GenericSection(sectionNumber,secLabel);
            section.setToolTip(toolTip);
            section.setIsEnabled(Boolean.parseBoolean(isEnabled));
            Integer sectionOrder = new Integer(secOrder);
            section.setSectionOrder(sectionOrder);

            sections.add(section);
        }
        return sections;
    }

    /**
     * Get list of root sections associated with the concept provided
     * @return
     */
    public List<Section> getRootSectionsWithConcept(Id concept){
        List<Section> sections = new ArrayList<>();

        String query = Queries.queryToGetRootSectionsWithConcept(concept, sectionNum, sectionLabel, sectionEnabled, positionOrder, sectionToolTip);
        List<QuerySolution> results = queryService.executeQuery(query);

        for (QuerySolution soln : results) {
            String sectionNumber = soln.get(sectionNum).toString();
            String secLabel = soln.get(sectionLabel).toString();
            String toolTip = soln.get(sectionToolTip).toString();
            String isEnabled = soln.get(sectionEnabled).toString();
            String secOrder = (soln.get(positionOrder)==null)?sectionNumber:soln.get(positionOrder).toString();

            CompositeSection section = new CompositeSection(sectionNumber, secLabel);
            section.setToolTip(toolTip);
            section.setIsEnabled(Boolean.parseBoolean(isEnabled));
            Integer sectionOrder = new Integer(secOrder);
            section.setSectionOrder(sectionOrder);

            sections.add(section);
        }
        return sections;
    }

    /**
     * Get list of all root sections.
     * 
     * @return list of sections
     */
    public List<Section> getAllRootSections(){
        List<Section> sections = new ArrayList<>();

        String query = Queries.queryToGetAllRootSections(sectionNum, sectionLabel, sectionEnabled, positionOrder, sectionToolTip);
        List<QuerySolution> results = queryService.executeQuery(query);

        for (QuerySolution soln : results) {
            String sectionNumber = soln.get(sectionNum).toString();
            String secLabel = soln.get(sectionLabel).toString();
            String toolTip = soln.get(sectionToolTip).toString();
            String isEnabled = soln.get(sectionEnabled).toString();
            String secOrder = (soln.get(positionOrder)==null)?sectionNumber:soln.get(positionOrder).toString();

            CompositeSection section = new CompositeSection(sectionNumber,secLabel);
            section.setToolTip(toolTip);
            section.setIsEnabled(Boolean.parseBoolean(isEnabled));
            Integer sectionOrder = new Integer(secOrder);
            section.setSectionOrder(sectionOrder);

            sections.add(section);
        }
        return sections;

    }

    /**
     * Get list of sub sections associated with section provided
     * @param section
     * @return list of sections
     */
    public List<Section> getSubSectionsForSection(Section section) {
        List<Section> sections = new ArrayList<>();
        String query = Queries.queryToGetSubSectionsOfSection(section, sectionNum, sectionLabel, sectionEnabled, 
            sectionToolTip, varIsFreeTextAllowed, positionOrder, uri);
        List<QuerySolution> results = queryService.executeQuery(query);

        for (QuerySolution soln : results) {
            RDFNode sectionPropNode = soln.get(uri) ;
            RDFNode sectionFreetextNode = soln.get(varIsFreeTextAllowed);

            String sectionNumber = soln.get(sectionNum).toString();
            String secLabel = soln.get(sectionLabel).toString();
            String toolTip = soln.get(sectionToolTip).toString();
            String isEnabled = soln.get(sectionEnabled).toString();
            Boolean isFreeTextAllowed = (sectionFreetextNode==null)?false:Boolean.parseBoolean(sectionFreetextNode.toString());
            String secOrder = soln.get(positionOrder).toString().trim();

            if(sectionPropNode==null || sectionPropNode.toString().isEmpty()){
                CompositeSection subsection = new CompositeSection(sectionNumber,secLabel);
                subsection.setToolTip(toolTip);
                subsection.setIsEnabled(Boolean.parseBoolean(isEnabled));
                subsection.setSectionOrder(new Integer(secOrder));
                subsection.setIsFreeTextAllowed(isFreeTextAllowed);
                sections.add(subsection);
            }else{
                GenericSection subsection = new GenericSection(sectionNumber,secLabel);
                subsection.setToolTip(toolTip);
                subsection.setIsEnabled(Boolean.parseBoolean(isEnabled));
                subsection.setIsFreeTextAllowed(isFreeTextAllowed);
                subsection.setSectionOrder(new Integer(secOrder));
                sections.add(subsection);
            }
        }
        return sections;
    }

    /**
     * Get list of properties associated with the section provided.
     * It is expected that at least section number is provided as part of this section.
     * 
     * @param section
     * @return list of properties
     */
    public List<Property> getPropertiesForSection(Section section){
        List<Property> properties = new ArrayList<Property>();
        String varPropUriForPharmml = QueryConst.URI.toString();
        String varPropUriForNonPharmml = QueryConst.NON_PHARMML_.toString()+QueryConst.URI;
        String varPropLabelForPharmml =  QueryConst.LABEL.toString();
        String varPropLabelForNonPharmml = QueryConst.NON_PHARMML_.toString()+QueryConst.LABEL;
        String query = Queries.queryToGetPropertiesForSection(section, varPropUriForPharmml, varPropUriForNonPharmml,
            varPropLabelForPharmml, varPropLabelForNonPharmml);

        List<QuerySolution> results = queryService.executeQuery(query);

        for (QuerySolution soln : results) {
            RDFNode pharmmlPropUriNode = soln.get(varPropUriForPharmml) ;
            RDFNode pharmmlPropLabelNode = soln.get(varPropLabelForPharmml) ;

            RDFNode nonPharmmlPropUriNode = soln.get(varPropUriForNonPharmml) ;
            RDFNode nonPharmmlPropLabelNode = soln.get(varPropLabelForNonPharmml) ;

            CompositeProperty resultPharmmlProperty = new CompositeProperty();
            CompositeProperty resultNonPharmmlProperty = new CompositeProperty();

            Id pharmmlPropId = new Id(pharmmlPropLabelNode.toString(), pharmmlPropUriNode.asNode().getURI());
            resultPharmmlProperty.setPropertyId(pharmmlPropId);
            resultPharmmlProperty.setPharmml(true);
            properties.add(getPropertyInformation(resultPharmmlProperty));

            Id nonPharmmlPropId = new Id(nonPharmmlPropLabelNode.toString(), nonPharmmlPropUriNode.asNode().getURI());
            resultNonPharmmlProperty.setPropertyId(nonPharmmlPropId);
            resultNonPharmmlProperty.setPharmml(false);
            properties.add(getPropertyInformation(resultNonPharmmlProperty));
        }
        return properties;
    }

    public Property getPropertyInformation(Property property){

        final String isRequired= QueryConst.IS_REQUIRED.toString();
        final String isEnabled= QueryConst.IS_ENABLED.toString();
        final String valSetType= QueryConst.VAL_SET_TYPE.toString();
        final String propRange= QueryConst.PROP_RANGE.toString();
        final String propUriFreeText = QueryConst.PROP_.toString()+QueryConst.FREETEXT;
        final String conceptLabel= QueryConst.CONCEPT_LABEL.toString();
        final String conceptUri= QueryConst.CONCEPT_URI.toString();

        String query = Queries.queryTogetPropertyInformation(property, isRequired, isEnabled, 
            valSetType, propRange, propUriFreeText, conceptLabel, conceptUri); 
        List<QuerySolution> results = queryService.executeQuery(query);

        for (QuerySolution soln : results) {
            RDFNode isReqdNode = soln.get(isRequired) ;
            RDFNode isEnableNode = soln.get(isEnabled) ;
            RDFNode rangeNode = soln.get(propRange) ;
            RDFNode valSetTypeNode = soln.get(valSetType) ;
            RDFNode propUriFreeTextNode = soln.get(propUriFreeText) ;
            RDFNode conceptLabelNode = soln.get(conceptLabel) ;
            RDFNode conceptUriNode = soln.get(conceptUri) ;

            CompositeProperty resultProperty = (CompositeProperty) property;

            resultProperty.setRequired(isPropertyRequired(isReqdNode.toString()));
            resultProperty.setEnabled(Boolean.parseBoolean(isEnableNode.toString()));
            resultProperty.setRange(PropertyRange.valueOf(rangeNode.toString()));
            resultProperty.setValueSetType(ValueSetType.valueOf(valSetTypeNode.toString().toUpperCase()));
            String freeText = (propUriFreeTextNode==null)?"":propUriFreeTextNode.toString();
            resultProperty.setFreeText(freeText);

            String conceptLabl = (conceptLabelNode==null)?"":conceptLabelNode.toString();
            Id conceptId = new Id(conceptLabl, conceptUriNode.asNode().getURI());
            resultProperty.setConceptId(conceptId);
        }
        return property;
    }

    private boolean isPropertyRequired(String requiredOption) {
        if(requiredOption.equals("always"))
            return true;
        else return false;
    }

    /**
     * Get ontology source/sources associated with the property provided.
     * 
     * @param property
     * @return list of ontology sources
     */
    public List<OntologySource> getOntologyForProperty(Property property) {
        final String propUri = "<"+property.getPropertyId().getUri()+">";
        final String sourceId = QueryConst.SOURCE.toString();

        if(property.getValueSetType().equals(ValueSetType.ONTOLOGY)){
            return getOntologyForProperty(propUri, label, sourceId);
        } else {
            throw new IllegalStateException("There is no ontology resource associated");
        }
    }

    /**
     * Gets list of values for the property depending upon uri and value set type of the property.
     * 
     * @param property
     * @return list of values
     */
    public List<Value> getValuesForProperty(Property property) {
        final String propUri = "<"+property.getPropertyId().getUri()+">";

        if(property.getValueSetType().equals(ValueSetType.TREE)){
            return getTreeValuesForProperty(propUri, label, uri);
        }else if(property.getValueSetType().equals(ValueSetType.LIST) 
                || property.getValueSetType().equals(ValueSetType.TEXT)){
            return getListValuesForProperty(propUri, label, uri);
        }else if(property.getValueSetType().equals(ValueSetType.TEXTLIST)){
            return getTextListValuesForProperty(propUri, label, uri);
        }else if(property.getValueSetType().equals(ValueSetType.ONTOLOGY)){
            throw new IllegalArgumentException("The 'ONTOLOGY' valueset type is supported by method 'findOntologyResourcesForProperty(property)'.");
        }else {
            throw new IllegalStateException("There is no value type supported");
        }
    }

    private List<Value> getListValuesForProperty(final String propUri, final String valueLabel, final String valueUri) {
        List<Value> values = new ArrayList<Value>();
        String query = Queries.queryToGetListValuesForProperty(propUri, valueLabel, valueUri); 

        List<QuerySolution> results = queryService.executeQuery(query);

        for (QuerySolution soln : results) {
            RDFNode uriNode = soln.get(valueUri) ;
            RDFNode labelNode = soln.get(valueLabel) ;
            Id valueId = new Id(labelNode.toString(), uriNode.asNode().getURI());
            Value value = new SimpleValue(valueId);
            values.add(value);
        }
        return values;
    }

    private List<Value> getTextListValuesForProperty(final String propUri, final String valueLabel, final String valueUri) {
        List<Value> values = new ArrayList<Value>();
        String query = Queries.queryToGetTextListValuesForProperty(propUri, valueLabel, valueUri); 

        List<QuerySolution> results = queryService.executeQuery(query);

        for (QuerySolution soln : results) {
            RDFNode labelNode = soln.get(valueLabel) ;
            Id valueId = new Id(labelNode.toString(), null);
            Value value = new SimpleValue(valueId);
            values.add(value);
        }
        return values;
    }

    private List<Value> getTreeValuesForProperty(final String propUri, final String valueLabel, final String valueUri) {
        Map<String, Value> treeValues = new TreeMap<String, Value>();
        final String valueRank = QueryConst.VAL_RANK.toString();
        String query = Queries.queryToGetTreeValuesForProperty(propUri, valueLabel, valueUri, valueRank); 

        List<QuerySolution> results = queryService.executeQuery(query);

        for (QuerySolution soln : results) {
            RDFNode uriNode = soln.get(valueUri) ;
            RDFNode labelNode = soln.get(valueLabel) ;
            RDFNode rankNode = soln.get(valueRank);
            Id valueId = new Id(labelNode.toString(), uriNode.asNode().getURI());
            Value value = new CompositeValue(valueId, rankNode.toString());
            treeValues.put(rankNode.toString(), value);
        }
        return arrangeTreeValues(treeValues);
    }

    private List<Value> arrangeTreeValues(Map<String, Value> values){
        List<Value> sortedValues = new ArrayList<Value>();

        for(String valueRank: values.keySet()){
            Value treeValue = values.get(valueRank);
            String[] rankSequence = valueRank.split("\\.");

            if(rankSequence.length==1){
                sortedValues.add(treeValue);
            }else{
                CompositeValue nextVal = (CompositeValue) values.get(rankSequence[0]);
                for(int i=1;i<rankSequence.length-1;i++){
                    nextVal = (CompositeValue) values.get(rankSequence[i]); 
                }
                nextVal.addValue(treeValue);
            }
        }
        return sortedValues;
    }

    private List<OntologySource> getOntologyForProperty(final String propUri, final String var_OntologyResource, final String var_OntologyId) {
        List<OntologySource> sources = new ArrayList<OntologySource>();
        String query = Queries.queryToGetOntologySourceForProperty(propUri, var_OntologyResource, var_OntologyId); 

        List<QuerySolution> results = queryService.executeQuery(query);
        for (QuerySolution soln : results) {
            RDFNode resourceNode = soln.get(var_OntologyResource) ;
            RDFNode idNode = soln.get(var_OntologyId) ;

            String resource = resourceNode.toString();
            String sourceId = idNode.toString();
            OntologySource ontSource = new OntologyResourceImpl(resource, sourceId);
            sources.add(ontSource);
        }
        return sources;
    }
}
