/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.sparql;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.common.base.Preconditions;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;

/**
 * Sparql query service provides functions to query upon RDF store service endpoint configured.
 */
public class SparqlQueryService {

    private String serviceUrl;

    public SparqlQueryService(String serviceUrl){
        this.serviceUrl = serviceUrl;
    }

    /**
     * This method verifies service url connection and throws illegal state exception if service url is not valid.
     * 
     * @param service Url to be verified
     */
    public void healthCheck(){
        Preconditions.checkNotNull(serviceUrl, "Service url cannot be null.");

        try{
            testServiceConnection(serviceUrl);
        } catch (IOException e) {
            throw new IllegalStateException("Connection cannot be verified for service URL provided. "+ e);
        }
    }

    private void testServiceConnection(String serviceUrl) throws ClientProtocolException, IOException{
        HttpClient httpClient = new DefaultHttpClient();  
        HttpGet connection = new HttpGet(serviceUrl);

        HttpResponse response = httpClient.execute(connection);
        int code = response.getStatusLine().getStatusCode();

        if(code == 401){
            throw new IllegalStateException("Connection cannot be authorized for service URL provided.");
        }else if(code != 200){
            throw new IllegalStateException("Connection cannot be verified for service URL provided. ");
        }
    }

    /**
     * Executes provided query on remote service over http.
     *  
     * @param queryString
     * @return resultSet with execution results
     */
    public List<QuerySolution> executeQuery(String queryString){
        try(QueryExecution qexec = QueryExecutionFactory.sparqlService(serviceUrl, queryString)){
            ResultSet results = qexec.execSelect();
            return ResultSetFormatter.toList(results);
        }
    }
}
