/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.sparql;

/**
 * Query constants are constants used while creating Sparql query. 
 * These arrangements are added as initial step towards handling sparql queries output.  
 */
public enum QueryConst {
    //id
    LABEL, URI,
    //value
    VAL_, VAL_SET_TYPE, VAL_RANK,
    //Ontology Source
    SOURCE,
    //CompositeProperty
    PROP_, PROP_RANGE,FREETEXT, IS_REQUIRED, IS_ENABLED, NON_PHARMML_, 
    CONCEPT_LABEL, CONCEPT_URI,
    //Section
    SEC_, SEC_NUM, TOOLTIP, ORDER;
}
