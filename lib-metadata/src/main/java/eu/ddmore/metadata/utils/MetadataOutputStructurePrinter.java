/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

/**
 * Creates metadata output structure for environment specified and store it to a file. 
 */
public class MetadataOutputStructurePrinter {

    private final String ENV_PROPERTIES = "/env.properties";

    private final String WORKING_DIR = "target"+File.separator+"output_structure"+File.separator;

    /**
     * The main method to execute metadata output structure as command line server to create and store output structure.
     * 
     *  usage : ?>MetadataOutputStructurePrinter <environmentName>
     *  
     * @param args parameter arguments
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        MetadataOutputStructurePrinter outputStructurePrinter = new MetadataOutputStructurePrinter();

        if(args.length==0 || args[0].isEmpty()){
            throw new IllegalArgumentException("Environment needs to be specified as parameter.");
        }

        String env = args[0];
        outputStructurePrinter.createAndPopulateOuputStructureFileForEnv(env);
        System.out.println("Output structure file is created successfully for "+ env);
    }

    /**
     * Metadata output structure to create and store output structure.
     * 
     * @param environmentName environment name (e.g. DEV)
     * @throws IOException
     */
    public void createAndPopulateOuputStructureFileForEnv(String environmentName) throws IOException{
        String url = getEnvironmentUrl(environmentName);
        OutputStructureBuilder structureBuilder = new OutputStructureBuilder();
        StringBuilder outputStructure = structureBuilder.getAllValuesForPopulatedRootSections(url);
        writeOutputStructureToFile(environmentName, outputStructure);
    }

    private String getEnvironmentUrl(String env) throws IOException{
        LocalPropertyAccessor propertAccessor = new LocalPropertyAccessor();
        Properties props = propertAccessor.loadProperties(ENV_PROPERTIES);
        String url = propertAccessor.getPropertyFor(props, env.toUpperCase());
        if (StringUtils.isEmpty(url)){
            throw new IllegalArgumentException("Environment specified is not valid or not from env properties configured .");
        }
        return url;
    }

    private void writeOutputStructureToFile(String env, StringBuilder outputStructure) throws IOException{
        File outputFile = new File(WORKING_DIR+env+"_OutputStructure_"+getCurrentDateStamp()+".txt");

        outputFile.getParentFile().mkdirs();
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "utf-8"))) {
            writer.write(OutputStructureBuilder.endline()+OutputStructureBuilder.endline("ENVIRONMENT : "+env));
            writer.write(OutputStructureBuilder.endline()+outputStructure.toString());
        }
    }

    private String getCurrentDateStamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy_hhmmss");
        Date currentDate = new Date();
        String stringDate = dateFormat.format(currentDate);
        return stringDate;
    }
}
