/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.utils;

import java.io.IOException;
import java.util.Properties;

import com.google.common.base.Preconditions;

/**
 * This class helps to access properties file specified and also to get values from properties file. 
 */
public class LocalPropertyAccessor {

    /**
     * Loads specified properties file from default project resources and returns as properties object. 
     * @param propertyString name of property file
     * @return properties object
     * @throws IOException
     */
    public Properties loadProperties(String propertyString) throws IOException{
        Properties props = new Properties();
        props.load(getClass().getResourceAsStream(propertyString));
        return props;
    }

    /**
     * Returns property value for the symbol from the properties provided as parameter.
     * If property doesn't exist it returns empty result.
     * @param properties properties to be looked into
     * @param symbol to get associated value from properties.
     * @return value associated with the symbol provided.
     */
    public String getPropertyFor(Properties properties, String symbol){
        Preconditions.checkNotNull(properties, "properties cannot be null.");
        Preconditions.checkNotNull(symbol, "symbol cannot be null.");
        if(properties.stringPropertyNames().contains(symbol)){
            return properties.getProperty(symbol);
        }else{
            return "";
        }
    }

}
