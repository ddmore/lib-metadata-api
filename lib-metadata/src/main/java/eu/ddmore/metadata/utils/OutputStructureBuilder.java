/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.utils;

import java.util.List;

import ontologies.OntologySource;
import eu.ddmore.metadata.api.MetadataInformationService;
import eu.ddmore.metadata.api.domain.enums.ValueSetType;
import eu.ddmore.metadata.api.domain.properties.Property;
import eu.ddmore.metadata.api.domain.sections.CompositeSection;
import eu.ddmore.metadata.api.domain.sections.GenericSection;
import eu.ddmore.metadata.api.domain.sections.Section;
import eu.ddmore.metadata.api.domain.values.Value;
import eu.ddmore.metadata.impl.MetadataInformationServiceImpl;
import eu.ddmore.metadata.sparql.SparqlQueryExecutor;
import eu.ddmore.metadata.sparql.SparqlQueryService;

/**
 * Output structure builder builds hierarchical structure with populated details of root sections for environment specified.
 *  
 */
public class OutputStructureBuilder {

    private static final String NEW_LINE = System.getProperty("line.separator");

    private static final String TOPSECTION = "TOP-SECTION";
    private static final String SUBSECTION = "SUB-SECTION";
    private static final String LEAFSECTION = "LEAF-SECTION";

    private final String doubleIndentFormat = endline(indent("%s",2));
    private final String tripleIndentFormat = endline(indent("%s",3));

    /**
     * This method gets all values for root sections populated and creates hierarchical structure with details.
     * 
     * @param url data store environment url 
     * @return StringBuilder with output structure
     */
    public StringBuilder getAllValuesForPopulatedRootSections(String url){
        SparqlQueryService queryService = new SparqlQueryService(url); 
        SparqlQueryExecutor executor = new SparqlQueryExecutor(queryService);
        MetadataInformationService metadataInfo = new MetadataInformationServiceImpl(executor);
        List<Section> sections = metadataInfo.getAllPopulatedRootSections();

        StringBuilder outputStructureSections = new StringBuilder();
        if(sections.isEmpty()){
            outputStructureSections.append("No root Sections found for this environment. Please check the data store and connection to server.");
        }
        outputStructureSections.append(endline("Sections : "));
        outputStructureSections.append(populateSections(TOPSECTION,sections, metadataInfo));
        return outputStructureSections;
    }

    private StringBuilder populateSections(String sectionType, List<Section> sections, MetadataInformationService metadataInfo){
        StringBuilder sectionDetails = new StringBuilder();
        for(Section section : sections){
            if(section.isSectionWithProperties()){
                sectionDetails.append(endline()+endline(LEAFSECTION+" :----------"));
            }else{
                sectionDetails.append(endline()+endline(sectionType+" :----------"));
            }
            String tootip = (section.getToolTip().trim().isEmpty())?"N/A":"\""+section.getToolTip()+"\"";

            sectionDetails.append(endline(indent(section.getSectionNumber()+"   : "+section.getSectionLabel())));
            sectionDetails.append(endline(indent(" Is Enabled           : "+section.isEnabled())));
            sectionDetails.append(endline(indent(" Is Freetext allowed  : "+section.isFreeTextAllowed())));
            sectionDetails.append(endline(indent(" Tooltip if available : "+tootip)));
            sectionDetails.append(endline(indent(" Order                : "+section.getSectionOrder())));

            if(!section.isSectionWithProperties()){
                List<Section> subsections = ((CompositeSection) section).getSections();
                sectionDetails.append(populateSections(SUBSECTION,subsections, metadataInfo));
            }
            else{
                sectionDetails.append(populateProperties(((GenericSection) section).getPharmmlProperties(), metadataInfo));
                sectionDetails.append(populateProperties(((GenericSection) section).getNonPharmmlProperties(), metadataInfo));
            }
        }
        return sectionDetails;
    }

    private StringBuilder populateProperties(List<Property> properties, MetadataInformationService metadataInfo){
        StringBuilder propertyDetails = new StringBuilder();

        if(!properties.isEmpty()){
            for(Property property : properties){
                propertyDetails.append(endline()+String.format(doubleIndentFormat, " PROPERTY :"));
                appnendPropertyDetails(propertyDetails, property);
                if(property.getValueSetType().equals(ValueSetType.ONTOLOGY)){
                    List<OntologySource> sources = metadataInfo.findOntologyResourcesForProperty(property);
                    for(OntologySource val : sources ){
                        propertyDetails.append(String.format(tripleIndentFormat, " Ontology Resource  : "+val.getResource()));
                        propertyDetails.append(String.format(tripleIndentFormat, " Ontology Source id : "+val.getSourceId()));
                    }
                }else {
                    List<Value> values = metadataInfo.findValuesForProperty(property);
                    propertyDetails.append(String.format(tripleIndentFormat, " Total Values associated : "+values.size()));
                    if(values.size()!=0){
                        for(Value val : values){
                            appendValueDetails(propertyDetails, val);
                            if(val.isValueTree())
                                propertyDetails.append(String.format(tripleIndentFormat, " Value Rank in Tree : "+val.getValueRank()));
                        }
                    }
                }
            }
        }
        return propertyDetails;
    }

    private void appendValueDetails(StringBuilder valueOutputStructure, Value val) {
        valueOutputStructure.append(endline()+String.format(tripleIndentFormat, " VALUE :"));
        valueOutputStructure.append(String.format(tripleIndentFormat, " Value Label : "+val.getValueId().getLabel()));
        valueOutputStructure.append(String.format(tripleIndentFormat, " Value URI   : "+val.getValueId().getUri()));
        valueOutputStructure.append(String.format(tripleIndentFormat, " is Tree     : "+val.isValueTree()));
    }

    private void appnendPropertyDetails(StringBuilder propertyOutputStructure, Property prop) {
        propertyOutputStructure.append(String.format(doubleIndentFormat, " Property Label   : "+prop.getPropertyId().getLabel()));
        propertyOutputStructure.append(String.format(doubleIndentFormat, " Property URI     : "+prop.getPropertyId().getUri()));
        propertyOutputStructure.append(String.format(doubleIndentFormat, " Property Range   : "+prop.getRange()));
        propertyOutputStructure.append(String.format(doubleIndentFormat, " Is Required      : "+prop.isRequired()));
        propertyOutputStructure.append(String.format(doubleIndentFormat, " Is Enabled       : "+prop.isEnabled()));
        propertyOutputStructure.append(String.format(doubleIndentFormat, " Is pharmml       : "+prop.isPharmml()));
        propertyOutputStructure.append(String.format(doubleIndentFormat, " Freetext         : "+prop.getFreeText()));
        propertyOutputStructure.append(String.format(doubleIndentFormat, " Value Type       : "+prop.getValueSetType()));

        propertyOutputStructure.append(String.format(doubleIndentFormat, " Concept Label    : "+prop.getConceptId().getLabel()));
        propertyOutputStructure.append(String.format(doubleIndentFormat, " Concept Uri      : "+prop.getConceptId().getUri()));
    }

    /**
     * Add indentation before the text provided. 
     * 
     * @param text to indent
     * @return text with indentation
     */
    public static String indent(String text) {
        return "\t" + text;
    }

    /**
     * Add multiple indentations before text depending upon number specified.
     * 
     * @param text to indent
     * @param numberOfIndents number of indents to be added before.
     * @return text with indentation
     */
    public static String indent(String text, int numberOfIndents) {
        String indentedText = text;
        for(int i=0;i<numberOfIndents;i++){
            indentedText ="\t" + indentedText;
        }
        return indentedText;
    }

    /**
     * Add endline after the text provided 
     * 
     * @param text to format
     * @return text with endline added
     */
    public static String endline(String text) {
        return text + endline();
    }

    /**
     * Return new line specific to system
     * 
     * @return "new line" string
     */    
    public static String endline() {
        return NEW_LINE;
    }
}
