/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import ontologies.OntologySource;

import org.junit.Before;
import org.junit.Test;

import eu.ddmore.metadata.MetadataOutputStructureAT.RepoEnvironment;
import eu.ddmore.metadata.api.MetadataInformationService;
import eu.ddmore.metadata.api.domain.Id;
import eu.ddmore.metadata.api.domain.enums.ValueSetType;
import eu.ddmore.metadata.api.domain.properties.CompositeProperty;
import eu.ddmore.metadata.api.domain.properties.Property;
import eu.ddmore.metadata.api.domain.sections.CompositeSection;
import eu.ddmore.metadata.api.domain.sections.GenericSection;
import eu.ddmore.metadata.api.domain.sections.Section;
import eu.ddmore.metadata.api.domain.values.Value;
import eu.ddmore.metadata.impl.MetadataInformationServiceImpl;
import eu.ddmore.metadata.sparql.SparqlQueryExecutor;
import eu.ddmore.metadata.sparql.SparqlQueryService;
import eu.ddmore.metadata.utils.LocalPropertyAccessor;

/**
 * Acceptance test for metadata information service which executes tests against UAT 
 */
public class MetadataInformationServiceAT {

    Id modelConcept = new Id("Model","http://www.pharmml.org/ontology/PHARMMLO_0000001");
    String cmdSectionNumber = "2.1";
    String cmdSectionLabel = "Context of model development";
    MetadataInformationService metadataInfo;

    @Before
    public void setUp() throws Exception {

        LocalPropertyAccessor propAccessor = new LocalPropertyAccessor();
        Properties properties = propAccessor.loadProperties("/env.properties");
        String queryUrl = propAccessor.getPropertyFor(properties, RepoEnvironment.UAT.toString());

        SparqlQueryService queryService = new SparqlQueryService(queryUrl); 
        SparqlQueryExecutor executor = new SparqlQueryExecutor(queryService);
        metadataInfo = new MetadataInformationServiceImpl(executor);
    }

    @Test
    public void shouldGetMetadataInfoForProvidedSection() {
        Section section = new CompositeSection(cmdSectionNumber,cmdSectionLabel);

        List<Section> sections = metadataInfo.getMetadataInformationFor(section);

        assertNotNull("sections should not be null", sections);
        Section resultingSection = sections.get(0);

        assertEquals("Should return concept correctly for section : "+resultingSection.getSectionNumber(), 
            cmdSectionLabel, ((GenericSection) resultingSection).getSectionLabel());

        assertFalse("sections should not be empty", sections.isEmpty());
    }

    @Test
    public void shouldGetSectionsForConcept(){

        List<Section> sections = metadataInfo.findSectionsForConcept(modelConcept);

        assertNotNull("sections should not be null", sections);
        assertFalse("sections should not be empty", sections.isEmpty());

        List<String> expectedSections = Arrays.asList
                ("2.4","3.3.7.3", "1.2", "1.5", "2.2", "3.3.7.2", "2.1", "2.5", "2.3", "4.6",
                    "1.6", "1.7", "1.10", "1.3", "1.8", "1.9", "1.4", "1.1");

        assertEquals("Should return all sections correctly for concept: "+"Model", 
            expectedSections.size(), sections.size());

        for(Section section : sections){
            System.out.println(section.getSectionNumber());
        }
        for(Section section : sections){
            assertTrue("resulting section with section number : "+section.getSectionNumber()+" is not expected one", expectedSections.contains(section.getSectionNumber().toString()));
        }
    }

    @Test
    public void shouldGetPropertiesForSection(){
        CompositeSection section = new CompositeSection(cmdSectionNumber,cmdSectionLabel);

        List<Property> props = metadataInfo.findPropertiesForSection(section);

        assertNotNull("properties should not be null", props);
        assertFalse("properties should not be empty", props.isEmpty());
    }

    @Test
    public void shouldGetOntologySourcesForProperty(){
        CompositeProperty property = new CompositeProperty();
        Id propertyId = new Id("has disease or indications", "http://www.pharmml.org/2013/10/PharmMLMetadata#model-related-to-disease-or-condition");
        property.setPropertyId(propertyId);
        property.setValueSetType(ValueSetType.ONTOLOGY);

        List<OntologySource> values= metadataInfo.findOntologyResourcesForProperty(property);

        assertNotNull("values should not be null", values);
        assertFalse("values should not be empty", values.isEmpty());
    }

    @Test
    public void shouldGetListValuesForProperty(){
        CompositeProperty property = new CompositeProperty();
        Id propertyId = new Id("modelling question", "http://www.pharmml.org/2013/10/PharmMLMetadata#model-modelling-question");

        property.setPropertyId(propertyId);
        property.setValueSetType(ValueSetType.LIST);

        List<Value> values= metadataInfo.findValuesForProperty(property);

        assertNotNull("values should not be null", values);
        assertFalse("values should not be empty", values.isEmpty());
    }

    @Test
    public void shouldGetTextValuesForProperty(){
        CompositeProperty property = new CompositeProperty();
        Id propertyId = new Id("Long technical model description", "http://www.pharmml.org/2013/10/PharmMLMetadata#model-has-description-long");

        property.setPropertyId(propertyId);
        property.setValueSetType(ValueSetType.TEXT);

        List<Value> values= metadataInfo.findValuesForProperty(property);

        assertNotNull("values should not be null", values);
        assertTrue("values should be empty", values.isEmpty());
    }

    @Test
    public void shouldGetTreeValuesForProperty(){

        CompositeProperty property = new CompositeProperty();
        Id propertyId = new Id("nature of research", "http://www.pharmml.org/2013/10/PharmMLMetadata#model-research-stage");
        property.setPropertyId(propertyId);
        property.setValueSetType(ValueSetType.TREE);

        List<Value> values= metadataInfo.findValuesForProperty(property);

        assertNotNull("values should not be null", values);
        assertFalse("values should not be empty", values.isEmpty());
    }
}
