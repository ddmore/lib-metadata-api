/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import ontologies.OntologyResourceImpl;
import ontologies.OntologySource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.powermock.api.mockito.PowerMockito.when;

import eu.ddmore.metadata.api.MetadataInformationService;
import eu.ddmore.metadata.api.domain.Id;
import eu.ddmore.metadata.api.domain.properties.CompositeProperty;
import eu.ddmore.metadata.api.domain.properties.Property;
import eu.ddmore.metadata.api.domain.sections.CompositeSection;
import eu.ddmore.metadata.api.domain.sections.GenericSection;
import eu.ddmore.metadata.api.domain.sections.Section;
import eu.ddmore.metadata.api.domain.values.Value;
import eu.ddmore.metadata.impl.MetadataInformationServiceImpl;
import eu.ddmore.metadata.sparql.SparqlQueryExecutor;

/**
 * Unit test for metadata information service.
 */
@RunWith(PowerMockRunner.class)
public class MetadataInformationServiceTest {

    @Mock SparqlQueryExecutor executor;
    @Mock GenericSection section;
    @Mock CompositeSection rootSection;
    @Mock CompositeProperty property;
    @Mock Value value;

    MetadataInformationService informationService;

    Id modelConceptId = new Id("Model","http://www.pharmml.org/ontology/PHARMMLO_0000001");
    Id modelPropertyId = new Id("modelling question", "http://www.pharmml.org/2013/10/PharmMLMetadata#model-modelling-question");
    Id valueId = new Id("Mechanistic Understanding", "http://www.ddmore.org/ontologies/ontology/pkpd-ontology#pkpd_0006026");
    OntologySource ontSource = new OntologyResourceImpl("OLS", "doid");
    String cmdSectionNumber = "2.1";
    String rootSectionNumber = "2";

    @Before
    public void setUp() throws Exception {
        when(section.getSectionNumber()).thenReturn(cmdSectionNumber);
        when(section.isSectionWithProperties()).thenReturn(true);
        when(rootSection.getSectionNumber()).thenReturn(rootSectionNumber);
        when(rootSection.isSectionWithProperties()).thenReturn(false);
    }

    @Test
    public void shouldGetMetadataInformationForSection() {
        when(executor.getConceptForSection(section)).thenReturn(modelConceptId);

        informationService = new MetadataInformationServiceImpl(executor);
        List<Section> sections = informationService.getMetadataInformationFor(section);

        assertNotNull("Sections cannot be null",sections);
        assertFalse("sections  cannot be empty", sections.isEmpty());
    }

    @Test
    public void shouldFindSectionsForConcept() {
        List<Section> sections = new ArrayList<>();
        sections.add(section);

        when(executor.getSectionsForConcept(modelConceptId)).thenReturn(sections);

        informationService = new MetadataInformationServiceImpl(executor);
        List<Section> resultSections = informationService.findSectionsForConcept(modelConceptId);

        assertNotNull("Sections cannot be null",resultSections);

        boolean containsExpectedSection = false;
        for(Section sect : resultSections){
            if(sect instanceof GenericSection){
                containsExpectedSection =  cmdSectionNumber == sect.getSectionNumber();
            }
        }
        assertTrue("Resulting sections list should have expected section. ", containsExpectedSection);
    }

    @Test
    public void shouldFindRootSectionsForConcept() {
        List<Section> sections = new ArrayList<>();
        sections.add(rootSection);

        when(executor.getRootSectionsWithConcept(modelConceptId)).thenReturn(sections);

        informationService = new MetadataInformationServiceImpl(executor);
        List<Section> resultSections = informationService.findPopulatedRootSectionsForConcept(modelConceptId);

        assertNotNull("Sections cannot be null",resultSections);

        boolean containsExpectedSection = false;
        for(Section sect : resultSections){
            if(!sect.isSectionWithProperties()){
                containsExpectedSection = rootSectionNumber == sect.getSectionNumber();
            }
        }
        assertTrue("Resulting sections list should have expected section. ", containsExpectedSection);
    }

    @Test
    public void shouldFindAllRootSections() {
        List<Section> sections = new ArrayList<>();
        sections.add(rootSection);

        when(executor.getAllRootSections()).thenReturn(sections);

        informationService = new MetadataInformationServiceImpl(executor);
        List<Section> resultSections = informationService.getAllPopulatedRootSections();

        assertNotNull("Sections cannot be null",resultSections);

        boolean containsExpectedSection = false;
        for(Section sect : resultSections){
            if(!sect.isSectionWithProperties()){
                containsExpectedSection = rootSectionNumber == sect.getSectionNumber();
            }
        }
        assertTrue("Resulting sections list should have expected section. ", containsExpectedSection);
    }

    @Test
    public void shouldFindSubSectionsForSection() {
        List<Section> sections = new ArrayList<>();
        sections.add(section);

        when(executor.getSubSectionsForSection(rootSection)).thenReturn(sections);

        informationService = new MetadataInformationServiceImpl(executor);
        List<Section> resultSections = informationService.findSubSectionsForSection(rootSection);

        assertNotNull("Sections cannot be null",resultSections);

        boolean containsExpectedSection = false;
        for(Section sect : resultSections){
            if(sect.isSectionWithProperties()){
                containsExpectedSection = cmdSectionNumber == sect.getSectionNumber();
            }
        }
        assertTrue("Resulting sections list should have expected section. ", containsExpectedSection);
    }

    @Test
    public void shouldFindPropertiesForSection() {
        when(property.getPropertyId()).thenReturn(modelPropertyId);

        List<Property> properties = new ArrayList<>(); 
        properties.add(property);

        when(executor.getPropertiesForSection(section)).thenReturn(properties);

        informationService = new MetadataInformationServiceImpl(executor);
        List<Property> resultProperties = informationService.findPropertiesForSection(section);

        assertNotNull("Properties cannot be null",resultProperties);

        boolean containsExpectedProperty = false;
        for(Property prop : resultProperties){
            Id propId = prop.getPropertyId();
            containsExpectedProperty = (modelPropertyId.getLabel().equals(propId.getLabel()));
        }
        assertTrue("Resulting Properties list should have expected property. ", containsExpectedProperty);
    }

    @Test
    public void shouldFindOntologySourcesForProperty() {

        when(property.getPropertyId()).thenReturn(modelPropertyId);

        List<OntologySource> ontSources = new ArrayList<>();
        ontSources.add(ontSource);

        when(executor.getOntologyForProperty(property)).thenReturn(ontSources);

        informationService = new MetadataInformationServiceImpl(executor);
        List<OntologySource> resultOntSources = informationService.findOntologyResourcesForProperty(property);

        assertNotNull("Values cannot be null",resultOntSources);

        boolean containsExpectedSection = false;
        for(OntologySource val : resultOntSources){
            String resultSourceId = val.getResource();
            containsExpectedSection = (ontSource.getResource().equals(resultSourceId));
        }
        assertTrue("Resulting values list should have expected value. ", containsExpectedSection);

    }
    @Test
    public void shouldFindValuesForProperty() {
        when(property.getPropertyId()).thenReturn(modelPropertyId);

        when(value.getValueId()).thenReturn(valueId);

        List<Value> values = new ArrayList<>(); 
        values.add(value);

        when(executor.getValuesForProperty(property)).thenReturn(values);

        informationService = new MetadataInformationServiceImpl(executor);
        List<Value> resultValues = informationService.findValuesForProperty(property);

        assertNotNull("Values cannot be null",resultValues);

        boolean containsExpectedSection = false;
        for(Value val : resultValues){
            Id valId = val.getValueId();
            containsExpectedSection = (valueId.getLabel().equals(valId.getLabel()));
        }
        assertTrue("Resulting values list should have expected value. ", containsExpectedSection);
    }
}
