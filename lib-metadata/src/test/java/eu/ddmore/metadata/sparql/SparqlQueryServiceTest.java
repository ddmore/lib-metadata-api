/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.sparql;

import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

@RunWith(PowerMockRunner.class)
//@PrepareForTest(QueryExecutionFactory.class)
public class SparqlQueryServiceTest {

    @Mock Query query;
    @Mock ResultSet resultSet;
    @Mock QuerySolution querySolution;
    @Mock QueryExecution queryExec;

    SparqlQueryService queryService;
    String serviceUrl = "demo_url";
    String queryString = "select ?s ?p ?o where { ?s ?p ?o}";
    List<QuerySolution> querySolutions = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
    }

    @Ignore
    @Test
    public void shouldExecuteQuery() {
        mockStatic(QueryExecutionFactory.class);

        queryService = new SparqlQueryService(serviceUrl);

        when(QueryExecutionFactory.sparqlService(serviceUrl, queryString)).thenReturn(queryExec);
        when(queryExec.execSelect()).thenReturn(resultSet);

        querySolutions = queryService.executeQuery(queryString);

        assertNotNull("concept should not be null", querySolutions);
    }

    @Test(expected=IllegalStateException.class )
    public void shouldThrowExceptionForInvalidUrl() throws IOException {
        queryService = new SparqlQueryService(serviceUrl);
        queryService.healthCheck();
    }
}
