/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata.sparql;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
//import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

import static org.junit.Assert.*;
import eu.ddmore.metadata.api.domain.Id;
import eu.ddmore.metadata.api.domain.enums.PropertyRange;
import eu.ddmore.metadata.api.domain.enums.ValueSetType;
import eu.ddmore.metadata.api.domain.properties.CompositeProperty;
import eu.ddmore.metadata.api.domain.properties.Property;
import eu.ddmore.metadata.api.domain.sections.CompositeSection;
import eu.ddmore.metadata.api.domain.sections.GenericSection;
import eu.ddmore.metadata.api.domain.sections.Section;
import eu.ddmore.metadata.api.domain.values.Value;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
public class SparqlQueryExecutorTest {

    @Mock SparqlQueryService queryService;
    @Mock GenericSection section;
    @Mock CompositeSection rootSection;
    @Mock CompositeProperty property;
    @Mock Value value;
    @Mock QuerySolution soln;
    @Mock ResultSet resultSet;

    SparqlQueryExecutor executor;
    List<QuerySolution> querySolutions = new ArrayList<QuerySolution>();

    Id modelConceptId = new Id("Model","http://www.pharmml.org/ontology/PHARMMLO_0000001");
    Id modelPropertyId = new Id("modelling question", "http://www.pharmml.org/2013/10/PharmMLMetadata#model-modelling-question");
    Id valueId = new Id("Mechanistic Understanding", "http://www.ddmore.org/ontologies/ontology/pkpd-ontology#pkpd_0006026");
    Id nextValueId = new Id("Study Design Optimization", "http://www.ddmore.org/ontologies/ontology/pkpd-ontology#pkpd_0006028");
    String cmdSectionNumber = "2.1";
    String cmdSectionOrder = "1";
    String sectionTooltip = "tootip";
    String sectionEnabled = "true";
    String rootSectionNumber = "2";
    String sectionLabel = "Context of model development";

    final String label = QueryConst.LABEL.toString();
    final String uri = QueryConst.URI.toString();
    final String order = QueryConst.ORDER.toString();
    final String toolTip = QueryConst.TOOLTIP.toString();
    final String isEnabled = QueryConst.IS_ENABLED.toString();

    @Before
    public void setUp() throws Exception {
        executor = new SparqlQueryExecutor(queryService);
        when(section.getSectionNumber()).thenReturn(cmdSectionNumber);
        when(rootSection.getSectionNumber()).thenReturn(rootSectionNumber);
        querySolutions.add(soln);
    }

    @Test
    public void shouldGetConceptForSection() {
        String query = Queries.queryToGetConceptForSection(label,uri,cmdSectionNumber);

        RDFNode labelNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode uriNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);

        when(labelNode.asNode().getLiteral().toString()).thenReturn(modelConceptId.getLabel());
        when(uriNode.asNode().getURI()).thenReturn(modelConceptId.getUri());

        when(soln.get(label)).thenReturn(labelNode);
        when(soln.get(uri)).thenReturn(uriNode);

        when(queryService.executeQuery(query)).thenReturn(querySolutions);
        Id concept = executor.getConceptForSection(section);

        assertNotNull("concept should not be null", concept);
    }

    @Test
    public void shouldGetSectionsForConcept() {
        String sectionNum = QueryConst.SEC_NUM.toString();
        String query = Queries.queryToGetSectionForConcept(modelConceptId, sectionNum, label, isEnabled, order, toolTip);

        when(resultSet.hasNext()).thenReturn(true,false);
        RDFNode sectionNumNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionLabelNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionTooltipNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionEnabledNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionOrderNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);

        when(sectionNumNode.toString()).thenReturn(cmdSectionNumber);
        when(sectionLabelNode.toString()).thenReturn(sectionLabel);
        when(sectionTooltipNode.toString()).thenReturn(sectionTooltip);
        when(sectionEnabledNode.toString()).thenReturn(sectionEnabled);
        when(sectionOrderNode.toString()).thenReturn(cmdSectionOrder);

        when(soln.get(sectionNum)).thenReturn(sectionNumNode);
        when(soln.get(label)).thenReturn(sectionLabelNode);
        when(soln.get(toolTip)).thenReturn(sectionTooltipNode);
        when(soln.get(order)).thenReturn(sectionOrderNode);
        when(soln.get(isEnabled)).thenReturn(sectionEnabledNode);

        when(queryService.executeQuery(query)).thenReturn(querySolutions);

        List<Section> sections = executor.getSectionsForConcept(modelConceptId);

        assertNotNull("sections should not be null", sections);
    }

    @Test
    public void shouldGetAllRootSections(){
        String sectionNum = QueryConst.SEC_NUM.toString();
        String query = Queries.queryToGetAllRootSections( sectionNum, label, isEnabled, order, toolTip);

        when(resultSet.hasNext()).thenReturn(true,false);
        RDFNode sectionNumNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionLabelNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionTooltipNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionEnabledNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);

        when(sectionNumNode.toString()).thenReturn(rootSectionNumber.toString());
        when(sectionLabelNode.toString()).thenReturn(sectionLabel);
        when(sectionEnabledNode.toString()).thenReturn(sectionEnabled);

        when(soln.get(sectionNum)).thenReturn(sectionNumNode);
        when(soln.get(label)).thenReturn(sectionLabelNode);
        when(soln.get(toolTip)).thenReturn(sectionTooltipNode);
        when(soln.get(isEnabled)).thenReturn(sectionEnabledNode);

        when(queryService.executeQuery(query)).thenReturn(querySolutions);

        List<Section> sections = executor.getAllRootSections();

        assertNotNull("sections should not be null", sections);
    }

    @Test
    public void getRootSectionsWithConcept(){
        String sectionNum = QueryConst.SEC_NUM.toString();
        String query = Queries.queryToGetRootSectionsWithConcept(modelConceptId, sectionNum, label, isEnabled, order, toolTip);

        when(resultSet.hasNext()).thenReturn(true,false);
        RDFNode sectionNumNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionLabelNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionTooltipNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode sectionEnabledNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);

        when(sectionNumNode.toString()).thenReturn(rootSectionNumber.toString());
        when(sectionLabelNode.toString()).thenReturn(sectionLabel);
        when(sectionEnabledNode.toString()).thenReturn(sectionEnabled);

        when(soln.get(sectionNum)).thenReturn(sectionNumNode);
        when(soln.get(label)).thenReturn(sectionLabelNode);
        when(soln.get(toolTip)).thenReturn(sectionTooltipNode);
        when(soln.get(isEnabled)).thenReturn(sectionEnabledNode);

        when(queryService.executeQuery(query)).thenReturn(querySolutions);

        List<Section> sections = executor.getRootSectionsWithConcept(modelConceptId);

        assertNotNull("sections should not be null", sections);
    }

    @Test
    public void shouldGetPropertyInformation() {
        final String isRequired= QueryConst.IS_REQUIRED.toString();
        final String isEnabled= QueryConst.IS_ENABLED.toString();
        final String valSetType= QueryConst.VAL_SET_TYPE.toString();
        final String propRange= QueryConst.PROP_RANGE.toString();
        final String propUriFreeText = QueryConst.FREETEXT.toString();
        final String conceptLabel= QueryConst.CONCEPT_LABEL.toString();
        final String conceptUri= QueryConst.CONCEPT_URI.toString();

        when(property.getPropertyId()).thenReturn(modelPropertyId);
        String query = Queries.queryTogetPropertyInformation(property, isRequired, isEnabled, 
            valSetType, propRange, propUriFreeText, conceptLabel, conceptUri); 

        when(resultSet.hasNext()).thenReturn(true,false);
        RDFNode labelNode = mock(RDFNode.class);
        RDFNode uriNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);
        RDFNode isEnabledNode = mock(RDFNode.class);
        RDFNode isRequiredNode = mock(RDFNode.class);
        RDFNode valSetTypeNode = mock(RDFNode.class);
        RDFNode propRangeNode = mock(RDFNode.class);
        RDFNode conceptLabelNode = mock(RDFNode.class);
        RDFNode conceptUriNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);

        when(soln.get(uri)).thenReturn(uriNode);
        when(soln.get(label)).thenReturn(labelNode);
        when(soln.get(isEnabled)).thenReturn(isEnabledNode);
        when(soln.get(isRequired)).thenReturn(isRequiredNode);
        when(soln.get(valSetType)).thenReturn(valSetTypeNode);
        when(soln.get(propRange)).thenReturn(propRangeNode);
        when(soln.get(conceptLabel)).thenReturn(conceptLabelNode);
        when(soln.get(conceptUri)).thenReturn(conceptUriNode);

        when(uriNode.asNode().getURI()).thenReturn(modelPropertyId.getUri());
        when(labelNode.toString()).thenReturn(modelPropertyId.getLabel());
        when(isEnabledNode.toString()).thenReturn(Boolean.toString(true));
        when(isRequiredNode.toString()).thenReturn("always");
        when(valSetTypeNode.toString()).thenReturn(ValueSetType.LIST.toString());
        when(propRangeNode.toString()).thenReturn(PropertyRange.MULTIPLE_CONSTRAINED_CHOICE.toString());
        when(conceptUriNode.asNode().getURI()).thenReturn(modelConceptId.getUri());
        when(conceptLabelNode.toString()).thenReturn(modelConceptId.getLabel());

        when(queryService.executeQuery(query)).thenReturn(querySolutions);

        List<Property> properties = executor.getPropertiesForSection(section);

        assertNotNull("concept should not be null", properties);
    }

    @Test
    public void shouldGetValuesForProperty() {

        String query = Queries.queryToGetListValuesForProperty("<"+modelPropertyId.getUri()+">", label, uri);

        when(property.getPropertyId()).thenReturn(modelPropertyId);
        when(property.getValueSetType()).thenReturn(ValueSetType.LIST);

        when(resultSet.hasNext()).thenReturn(true,false);
        RDFNode labelNode = mock(RDFNode.class);
        RDFNode uriNode = mock(RDFNode.class, RETURNS_DEEP_STUBS);

        when(labelNode.toString()).thenReturn(valueId.getLabel());
        when(uriNode.asNode().getURI()).thenReturn(valueId.getUri());

        when(soln.get(label)).thenReturn(labelNode);
        when(soln.get(uri)).thenReturn(uriNode);

        when(queryService.executeQuery(query)).thenReturn(querySolutions);

        List<Value> values = executor.getValuesForProperty(property);

        assertNotNull("concept should not be null", values);
    }
}
