/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along 
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.metadata;

import java.io.IOException;

import org.junit.Test;

import eu.ddmore.metadata.utils.MetadataOutputStructurePrinter;

/**
 * This test queries over each environment from properties to create output structure and saves it in a file.
 */
public class MetadataOutputStructureAT {

    enum RepoEnvironment{
        PROD,
        DEV,
        UAT,
        LOCAL;
    }

    @Test
    public void shouldPrintMetaDataOutputStructureForDEV() throws IOException{
        MetadataOutputStructurePrinter outputStructurePrinter = new MetadataOutputStructurePrinter();
        outputStructurePrinter.createAndPopulateOuputStructureFileForEnv(RepoEnvironment.DEV.toString());
    }

    @Test
    public void shouldPrintMetaDataOutputStructureForUAT() throws IOException{
        MetadataOutputStructurePrinter outputStructurePrinter = new MetadataOutputStructurePrinter();
        outputStructurePrinter.createAndPopulateOuputStructureFileForEnv(RepoEnvironment.UAT.toString());
    }

    @Test
    public void shouldPrintMetaDataOutputStructureForPROD() throws IOException{
        MetadataOutputStructurePrinter outputStructurePrinter = new MetadataOutputStructurePrinter();
        outputStructurePrinter.createAndPopulateOuputStructureFileForEnv(RepoEnvironment.PROD.toString());
    }

    @Test
    public void shouldPrintMetaDataOutputStructureForLOCAL() throws IOException{
        MetadataOutputStructurePrinter outputStructurePrinter = new MetadataOutputStructurePrinter();
        outputStructurePrinter.createAndPopulateOuputStructureFileForEnv(RepoEnvironment.LOCAL.toString());
    }
}
